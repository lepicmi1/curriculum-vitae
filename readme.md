# BI-TEX Šablona pro životopis

Tato práce vznikla v rámci předmětu BI-TEX na FIT ČVUT. Zaměřením této práce bylo vytvořit šablonu pro životopis v TeXu.

Obsahem repozitáře je:
  img - složka pro portrétový obrázek
  cv.tex - makra šablony
  example.tex - příklad použití
  example.pdf - výsledek příkladu v pdf

Pro vytvoření výsledného životopisu je zapotřebí mít nainstalovaný font `fontawesome` z balíčku `texlive-fonts-extra` (může se lišit dle distribuce).

Následně spustit příkaz nad souborem `pdfcsplain example.tex`, který vygeneruje pdf.